package com.example.bo;

import com.example.dao.entity.user;

public class userBO {
	String UserID;
	String UserName;
	String Password;
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public userBO(String userID, String userName, String password) {
		UserID = userID;
		UserName = userName;
		Password = password;
	}
	public userBO(){
		
	}
	
	//Convert form entity to BO
	public static userBO convertfromEntity(user entity){
		return new userBO(entity.getUserID(),entity.getUserName(), entity.getPassword());
	}
	
	//Convert from BO to entity
	public static user converttoEntity(userBO BO){
		user us=new user();
		if(!BO.getUserID().isEmpty())
			us.setUserID(BO.getUserID());
		us.setUserName(BO.getUserName());
		us.setPassword(BO.getPassword());
		return us;
	}
	//Convert from BO to entity for insert
	public static user convertToInsert(userBO BO){
		user us=new user();
		us.setUserName(BO.getUserName());
		us.setPassword(BO.getPassword());
		return us;
	}
}
