package com.example.dao.reponsitory;

import java.util.List;

public interface AbstractDAO<T> {
	List<T> getAll();
	
	T insert(T entity);

	T update(T entity);

	T findbyID(String ID);

	T delete(String  ID);

}
