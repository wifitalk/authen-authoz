package com.example.dao.reponsitory.Impl;

import org.springframework.stereotype.Repository;

import com.example.dao.entity.user;
import com.example.dao.reponsitory.userDAO;

@Repository("userDAO") //Define this class Implement for userDAO when calling bean using @Autowired
public class userDAOImpl extends AbstractDAOImpl<user> implements userDAO {
	//Implement all method of userDAO extends by AbstractDAO using AbstractDAOImpl
}
