package com.example.dao.reponsitory.Impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.dao.DataAccessException;

import com.example.dao.reponsitory.AbstractDAO;

public class AbstractDAOImpl<T> implements AbstractDAO<T> {

	@Autowired
	private SessionFactory sessionFactory;
	private Class<T> EntityType;
	
	@SuppressWarnings("unchecked")
	public AbstractDAOImpl() {
		// get type class
	  EntityType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractDAOImpl.class);
	}
	
	
	
	@Override
	public List<T> getAll() {
		// TODO Auto-generated method stub
		// Getall class entity by using Critiria Query

		return getCurrentSession().createCriteria(EntityType).list();
	}

	@Override
	public T insert(T entity) {
		// TODO Auto-generated method stub
		//Insert entity to entity class using Critiria Query
		
		try {
		     getCurrentSession().persist(entity);
		     return entity;
	    } catch (DataAccessException ex) {
	      return null;
	    }
	}

	@Override
	public T update(T entity) {
		// TODO Auto-generated method stub
		//Update entity to entity class using Critiria Query
		
		try{
			getCurrentSession().update(entity);
			return entity;
		}catch(DataAccessException ex){
			return null;
		}
	}

	@Override
	public T findbyID(String ID) {
		// TODO Auto-generated method stub
		//Find one  entity in entity class using Critiria Query
		
		Object object = getCurrentSession().get(EntityType, ID);
		if (object != null) {
	      return EntityType.cast(object);
	    }
		return null;
	}

	@Override
	public T delete(String ID) {
		// TODO Auto-generated method stub
		//Delete entity to entity class using Critiria Query
		
		T find = findbyID(ID);
	    if (find != null) {
	      getCurrentSession().delete(find);
	    }
		return find; //find can null
	}

	public Session getCurrentSession() {
		 return sessionFactory.getCurrentSession();
	}
}
