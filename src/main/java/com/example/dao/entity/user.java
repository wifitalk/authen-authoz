package com.example.dao.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class user {
	
	@Id
	String UserID;
	@Column(name="UserName")
	String UserName;
	@Column(name="Password")
	String Password;
	
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public user(){
		//Random GUID
		UUID uuid = UUID.randomUUID();
		UserID=uuid.toString();
	}
}
