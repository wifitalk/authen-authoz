package com.example.services;

import java.util.List;

import com.example.bo.userBO;

public interface userServices {
	List<userBO> getAllUser();
	userBO	insert(userBO BO);
	userBO	update(userBO BO);
	userBO	delete(String userID);
	userBO findbyID(String userID);
}
