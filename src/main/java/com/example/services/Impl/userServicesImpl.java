package com.example.services.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bo.userBO;
import com.example.dao.entity.user;
import com.example.dao.reponsitory.userDAO;
import com.example.services.userServices;

@Service("userServices") //Define this class implement in Bean for userServices
@Transactional	//keep transaction for connect to DB

public class userServicesImpl implements userServices{

	@Autowired //Auto define and connect all bean involve userDAO
	private userDAO dao;
	
	@Override
	public List<userBO> getAllUser() {
		// TODO Auto-generated method stub
		//Convert each user to userBO and collect all in 1 list
		
		return (List<userBO>)dao.getAll().stream().map(c->userBO.convertfromEntity(c)).collect(Collectors.toList());
	}

	@Override
	public userBO insert(userBO BO) {
		// TODO Auto-generated method stub
		user us=dao.insert(userBO.convertToInsert(BO));
		if(us!=null)
			return userBO.convertfromEntity(us);
		return null;
	}

	@Override
	public userBO update(userBO BO) {
		// TODO Auto-generated method stub
		user us=dao.update(userBO.converttoEntity(BO));
		if(us!=null)
			return userBO.convertfromEntity(us);
		return null;
	}

	@Override
	public userBO delete(String userID) {
		// TODO Auto-generated method stub
		user us=dao.delete(userID);
		if(us!=null)
			return userBO.convertfromEntity(us);
		return null;
	}

	@Override
	public userBO findbyID(String userID) {
		// TODO Auto-generated method stub
		user us=dao.findbyID(userID);
		if(us!=null)
			return userBO.convertfromEntity(us);
		return null;
	}

}
