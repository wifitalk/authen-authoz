package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;
import com.example.bo.userBO;

import com.example.services.userServices;



@RestController				//Define this class is a controller
@RequestMapping("/user")	//Define url to call this class
public class userController {

		@Autowired	//Call services
		userServices service;
		
		//CRUD
		//Using Method GET 
		@RequestMapping(method = { RequestMethod.GET } )
		public ResponseEntity<List<userBO>> getAllUser(){
			return ResponseEntity.ok(service.getAllUser());
		}
		
		//Using Method Get and path to get 1 User
		@RequestMapping(path="/{userID}",method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<userBO> find(@PathVariable String userID){
			userBO bo=service.findbyID(userID);
			if(bo!=null)
				return ResponseEntity.ok(bo);
			return ResponseEntity.badRequest().body(bo);
		}
		
		//Using Method Post to add Data, DataType JSON
		@RequestMapping(method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<userBO> add(@RequestBody userBO BO){
			userBO bo=service.insert(BO);
			if(bo!=null)
				return ResponseEntity.ok(bo);
			return ResponseEntity.badRequest().body(BO);
		}
		
		//Using Method Delelte to delete 1 user by ID
		@RequestMapping(path="/{userID}",method = { RequestMethod.DELETE }, produces = { MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<userBO> delete(@PathVariable String userID){
			userBO bo=service.delete(userID);
			if(bo!=null){
				return ResponseEntity.ok(bo);
			}
			return ResponseEntity.badRequest().body(bo);
		}
		
		//Using Method PUT to Update 1 User
		@RequestMapping(method = { RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<userBO> update(@RequestBody userBO BO){
			userBO bo=service.update(BO);
			if(bo!=null)
				return ResponseEntity.ok(bo);
			return ResponseEntity.badRequest().body(bo);
		}
		
}
